<?php

/**
 * @file
 * Available API and hooks for File Drop
 *
 */

/**
* @addtogroup hooks
* @{
*/

/**
 * Invoked after file has been validated in _submit hook, and after the file id
 * has been associated with the node id in filedrop's table. Allows for modules
 * to do something once a file has been uploaded to a node.
 *
 * @param int $nid
 *   The node ID of the node that the file is being attached to.
 * @param object $file
 *   Standard file object as returned by file_save_upload(), contains the user
 *   ID of the uploader.
 */
function hook_filedrop_file_upload($nid, $file) {
  // This example loads the target node, and updates it with what user has uploaded
  $node = node_load($nid);
  $node->user_task_complete[$file->uid] = TRUE;
  node_save($node);
}

/**
 * Invoked after file has been deleted in _submit hook. Allows for modules
 * to do something once a file has been deleted from a node.
 *
 * @param int $nid
 *   The node ID of the node that has had the file removed from.
 * @param object $file
 *   Standard file object referencing deleted file.
 */
function hook_filedrop_file_delete($nid, $file) {
  // This example loads the target node, and disables the user complete status
  $node = node_load($nid);
  $node->user_task_complete[$file->uid] = FALSE;
  node_save($node);
}

/**
 * Invoked during node view for a file drop enabled node, return false to revoke
 * upload permission for the given user.
 *
 * @param int $nid
 *   The node ID of the node that a file could be uploaded to
 * @param int $uid
 *   The ID of the user whose permission to upload can be revoked
 */
function hook_filedrop_upload_access($nid, $uid) {
  // This example loads the target node, and revokes permission based on a field
  $node = node_load($nid);
  if (!$node->upload_per_user_allow[$file->uid]){
    return FALSE;
  }
}

/**
* @} End of "addtogroup hooks".
*/