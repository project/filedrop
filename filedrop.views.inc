<?php


/**
 * Implementation of hook_views_data().
 */
function filedrop_views_data() {
  $tables['filedrop_associations']['table']['group'] = t('Filedrop associations');

  // How is the filedrop table linked to the nodes
  $tables['filedrop_associations']['table']['join'] = array(
    'node' => array (
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'file' => array (
      'left_field' => 'fid',
      'field' => 'fid',
    ),
  );

  $tables['files']['table']['join']['users'] = array(
      'left_field' => 'uid',
      'field' => 'uid',
  );

  // description of the fields (table columns)
  $tables['filedrop_associations']['fid'] = array(
    'title' => t('File ID'),
    'help' => t('File ID of a file associated with a node'),
    'relationship' => array(
      'base' => 'files',
      'field' => 'fid',
      'handler' => 'views_handler_relationship',
      'label' => 'Filedrop File',
    ),
  );

  $tables['filedrop_associations']['nid'] = array(
    'title' => t('Node ID'),
    'help' => t('Node ID of a node associated with a file'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => 'Filedrop node',
    ),
  );

  return $tables;
}

/**
 * Implementation of hook_views_pre_render().
 */
function filedrop_views_pre_render(&$view) {
  foreach ($view->field as $key => $field_object) {
    if (strpos('views_handler_field_file', get_class($field_object)) === 0 && !empty($field_object->aliases['filepath'])) {
      foreach ($view->result as $row_key => $row) {
        $filename = $field_object->aliases['filepath'];
        if (!empty($row->$filename)) {
          filedrop_change_filepath($view->result[$row_key]->$filename);
        }
      }
    }
  }
}
